# Setup x-code command line tools
xcode-select --install

# Download/Install Homebrew
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"

# Tap into more casks
brew tap homebrew/cask-versions
brew tap homebrew/cask-fonts

# Download the apps
brew cask install firefox
brew cask install google-chrome

brew cask install visual-studio-code
brew cask install gitkraken
brew cask install iterm2

brew cask install slack
brew cask install discord
brew cask install whatsapp
brew cask install telegram
brew cask install discord-canary

brew cask install spotify
brew cask install vlc

brew cask install lastpass

# Install vs-code extension for sync
code --install-extension shan.code-settings-sync --force
